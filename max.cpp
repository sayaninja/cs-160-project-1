#include "max.hpp"

// You will need to implement a complete visitor that
// finds the maximum element in the tree. You can use
// the print and sum visitors for reference.

// WRITEME
void MaxVisitor::visitNode(Node* node) {
	int temp = node->value;
	if(max<temp)
		max = temp;
	node->visit_children(this);
}